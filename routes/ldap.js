module.exports = function(router, passport) {
    var async = require('async');
    var ActiveDirectory = require('activedirectory');
    var ad = new ActiveDirectory(require('../config'));
    
    
    /* GET home page. */
    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Node + Express + Active Directory' });
    });
    
    // show the login form
    router.get('/login', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { title: 'Node + Express + Active Directory', message: req.flash('loginMessage') }); 
    });
    
    router.get('/profile', isAuthenticated, function(req, res) {
        res.render('profile.ejs', {
            user : req.user, // get the user out of session and pass to template
            title: 'Node + Express + Active Directory'
        });
    });
    
    router.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    
    var LocalStrategy = require('passport-local').Strategy;
    
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
    
    
    passport.use('active-directory', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    }, function(request, username, password, done) {
        var user = null;
        async.series([
            function(proceed){
                ad.authenticate(username, password, function(err, auth){
                    if (err) {
                    console.log(err);
                    done(null, false, request.flash('loginMessage', 'Server error'));
                    } else if (auth) {
                    proceed();
                    } else {
                    done(null, false, request.flash('loginMessage', 'Invalid or unknown credentials'));
                    }
                
                });
            },
            function(proceed){
                ad.findUser(username.split('\\')[1], function(err, u){
                   if (err) {
                       console.log(err);
                       done(null, false, request.flash('loginMessage', 'Server error'));
                   } else {
                       u.username = u.sAMAccountName;
                       user = u;
                       proceed();
                   }
               });
            },
            function(proceed){
                ad.getGroupMembershipForUser(username.split('\\')[1], function(err, groups){
                   if (err) {
                       console.log(err);
                       done(null, false, request.flash('loginMessage', 'Server error'));
                   } else {
                       console.log(groups);
                       user.groups = groups;
                       proceed();
                   }
               });
            }
        ], function(){
            done(null, user);
        })

    }));
    
    router.post('/login', passport.authenticate('active-directory', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    
    
    // route middleware to check if use is authenticated
    function isAuthenticated(req, res, next) {
    
        // user is authenticated in the session
        if (req.isAuthenticated()) {
            return next();
        }
    
        // if they aren't redirect them to the home page
        res.redirect('/login');
    }

};