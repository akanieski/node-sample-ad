module.exports = function(router, passport) {
    var async = require('async');
    
    
    /* GET home page. */
    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Node + Express + Active Directory' });
    });
    
    router.get('/login',
        passport.authenticate('adfs', { failureRedirect: '/', failureFlash: true }),
            function(req, res) {
                res.redirect('/');
        }
    );
    
    router.get('/profile', isAuthenticated, function(req, res) {
        res.render('profile.ejs', {
            user : req.user, // get the user out of session and pass to template
            title: 'Node + Express + Active Directory'
        });
    });
    
    router.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    var SamlStrategy = require('passport-azure-ad').SamlStrategy;
    
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
        
    var config = {
        // required options
        identityMetadata: 'https://localhost/federationmetadata/2007-06/federationmetadata.xml',
        loginCallback: 'http://127.0.0.1:3000/login/callback/',
        issuer: 'http://127.0.0.1:3000',  
    };
    
    passport.use('adfs', new SamlStrategy(config, function(profile, done) {
        if (!profile.email) {
            return done(new Error("No email found"), null);
        }
        return done(null, profile);
    }));
    
    router.post('/login', passport.authenticate('adfs', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    router.post('/login/callback',
        passport.authenticate('saml', { failureRedirect: '/', failureFlash: true }),
        function(req, res) {
            res.redirect('/');
        }
    );
    
    // route middleware to check if use is authenticated
    function isAuthenticated(req, res, next) {
    
        // user is authenticated in the session
        if (req.isAuthenticated()) {
            return next();
        }
    
        // if they aren't redirect them to the home page
        res.redirect('/login');
    }

};